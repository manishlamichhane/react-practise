//import update from 'immutability-helper';
import PlayerRound from '../models/PlayerRound';


class RoundController {
    // NOTE: This method needs to implement Immutablity
    createNewRound(pointsDetail, roundId) { 
        // creates a new Round object
        const {playerDidShow, hasSeenMaal, pointsThisRound} = pointsDetail;
        
        // pointsThisRound is in string. Converting it to Number before creating Round object
        return new PlayerRound(roundId, playerDidShow, hasSeenMaal, Number(pointsThisRound));
    }

    calculatePlayerPointsPerRound(rounds, players) {
        for (let round of rounds) {
            this.calculate(round, players);
        }
        return [rounds, players]
    }

    calculate(roundObj, players) {
        let playerWhoDidShow;
        let pointsOfPlayerWhoDidShow = 0;
        for (let player of players) {
            if (player.round.length === 0)
                continue;
            
            let playerRound = player.round[roundObj.id - 1]; 
            let extraPointsForShow = 0;
            let numberOfPlayers = players.length;
            
            // do not process if this player did show
            // IMPORTANT TODO: Before Calculation, Make sure that there is a player who did show
            if(playerRound.didShow){
                playerWhoDidShow = player;
                continue;
            } 

            else if (playerRound.hasSeenMaal === true) {
                extraPointsForShow = 3; // if player has seen maal, player who did show receives only 3 extra points. 
            }
            else if (playerRound.hasSeenMaal === false) {
                extraPointsForShow = 10; // if current player has seen not seen maal, player who did show receives 10 extra points. 
            }
            
            // since JS passes by reference, this mutates the origianl player.round arrays specific round object
            // todo: refactor it to make things immutable 
            // intentionally making this value negative so that it indicates a player has to pay this sum to the one who did show
            playerRound.pointsInThisRoundAfterDeductions = (playerRound.pointInThisRound * numberOfPlayers) - (roundObj.totalPoints + extraPointsForShow);
            
            pointsOfPlayerWhoDidShow += playerRound.pointsInThisRoundAfterDeductions;
            // calculates the cummulative total
            this.calculateCummulativePoints(roundObj, player)
        }
        // the point of player who did show in a given round is the negative net sum of points of all players
        playerWhoDidShow.round[roundObj.id - 1].pointsInThisRoundAfterDeductions = -(pointsOfPlayerWhoDidShow);
        this.calculateCummulativePoints(roundObj, playerWhoDidShow)
    }

    calculateCummulativePoints(round, player) {
        let currentRound = player.round[round.id - 1]
        if (currentRound.roundId === 1) {
            currentRound.totalCummulativePointsAfterDeduction = currentRound.pointsInThisRoundAfterDeductions;
            return 0;
        }

        let previousRound = player.round[round.id - 2]
        
        let cummulativeTotal = previousRound.totalCummulativePointsAfterDeduction + currentRound.pointsInThisRoundAfterDeductions
        currentRound.totalCummulativePointsAfterDeduction = cummulativeTotal;
    }
}

export default RoundController;
