

export default class StyleController {
    createSmallRedSpan(point, totalPoints=0) {
        let pointsDisplay = `${point} (${totalPoints})`;
        if (point < 0)
            return <span style={{ color: "red" }}> <small>{pointsDisplay}</small></span>;
        return <span style={{ color: "green" }}> <small>{pointsDisplay}</small></span>;
    }
}
