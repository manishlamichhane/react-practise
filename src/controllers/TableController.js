import StyleController from "./StyleController";
import MarriageTable from "../components/marriage/MarriageTable";

class TableController {
    constructor(numberOfRounds, players, showPointsModal, updatePlayerName) {
        this.numberOfRounds = numberOfRounds;
        this.players = players;
        this.styleController = new StyleController();
        // TODO: Find a less hacky way of updating player name
        // this is a binding to AdvanceTable's updatePlayerName methods
        this.showPointsModal = showPointsModal;
        this.updatePlayerName = updatePlayerName;
    }

    createHeaderRow() {
        // creates header row for the board
        let columns = [];
        
        // creates the Round column in Table Header
        let roundHeaderColumn = <th key="0" >Rounds</th>;
        columns.push(roundHeaderColumn);

        for(let player of this.players) {
            let column = this.createHeaderColumn(player);
            columns.push(column);
        }
        
        return <tr key="0">{columns}</tr>
    }

    createHeaderColumn(player) {
        // creates a single Header Column and puts the value provided in the innerHTML
        return <th key={player.id} onClick={(e) => this.updatePlayerName(player)}>{player.name}</th>
    }
    
    createRows() {
        // creates a number of rows based on the argument provided 
        let rows = [];
        for(let roundNumber = 1; roundNumber <= this.numberOfRounds; roundNumber++) {
            // headerRow has a key 0. So second row will need to have a key of 1
            let row = this.createRow(roundNumber);
            rows.push(row);
        }
        return rows;
    }

    createRow(roundNumber) {
        // creates a single row with a number of colums as provided in props
        // and a column representating Round number
        let columns = [];

        // creates the column representating the "Round Number" text
        let roundNumberColumn = <td key={`${roundNumber}0`} >{roundNumber}</td>;
        columns.push(roundNumberColumn);

        for(let player of this.players) {
            // roundNumber is 1 plus the index. So to get the round we need to
            // round[roundNumber - 1] 
            let maalDisplay;
            let pointsDisplay = "-"
            let totalPoints = 0;
            if (player.round[roundNumber - 1] !== undefined) {
                let round = player.round[roundNumber - 1]
                if (round.hasSeenMaal)
                    maalDisplay = round.pointInThisRound;
                else
                    maalDisplay = 0;
                pointsDisplay = round.pointsInThisRoundAfterDeductions
                totalPoints = round.totalCummulativePointsAfterDeduction;
            } else {
                maalDisplay = "Click to Add";
            }

            let column = this.createColumn(roundNumber, player, maalDisplay, pointsDisplay, totalPoints);
            columns.push(column);
        }

        return <tr key={roundNumber}>{columns}</tr>
    }

    createColumn(roundNumber, player, maal, points, totalPoints) {
        // creates a single columns and puts the value provided, in innerHTML.
        // the roundNumber and playerNumber are very important as they determine
        // the flow of input from Modal to the respective Player in the respective round. 
        let roundPlayerIdentifier = `${roundNumber}${player.id}`
        let playerDidShowInThisRound = player?.round[roundNumber - 1]?.didShow || false; 
        let props = {
            key: roundPlayerIdentifier, 
            onClick: (e) => this.showPointsModal(roundNumber, player.id),
        }
    
        if (playerDidShowInThisRound === true)
            props.style = {backgroundColor: "pink"};

        return <td {...props}>
            {maal}
            <br />
            {this.styleController.createSmallRedSpan(points, totalPoints)}{' '}
            </td>
    }

    createTable() {
        let rows = this.createRows();
        let header = this.createHeaderRow();
        return <MarriageTable header={header} rows={rows} />
    }
}

export default TableController;