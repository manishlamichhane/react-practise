import Player from '../models/Players';
import update from 'immutability-helper';

export default class PlayerController {
    constructor(numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        this.defaultPrefix = 'Pl.';
    }

    initialize() {
        // initializes players objects
        let playersList = [];
        let columnHeaderNames = this.getDummyColumnHeaderNames();
        
        for (let i=0; i < columnHeaderNames.length; i++) {
            // the playerIds need to start from 1 and not 0
            // the playerIds is used as unique keys to the <th key={player.id}></th> elements
            // the first th element has key 0 <th key="0"></th>
            // hence the players start from 1
            let playerId = i + 1;
            let player = new Player(playerId, columnHeaderNames[i]);
            playersList.push(player);
        }

        return playersList;
    }

    getDummyColumnHeaderNames() {
        // returns dummy column header names for initial game board setup
        let dummyNames = [];
        for (let i = 1; i <= this.numberOfPlayers; i++){
            let dummyName = `${this.defaultPrefix} ${i}`;
            dummyNames.push(dummyName);
        }

        return dummyNames;
    }

    updatePlayerName = (player) => {
        let newName = prompt('Enter new Name: ', player.name);
        
        // adhering to immutability
        // even though the name of a player is changed, we replace the entire players state
        let updatedPlayer = update(player, {name: {$set: newName}});
        
        return updatedPlayer
    }
}
