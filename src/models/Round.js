class Round {
    // Represents player's data in a particular round
    constructor(roundId, totalPoints, playerHasShown=false){
        this.id = roundId;

        // represents total number of points from all players in a particular round
        this.totalPoints = totalPoints;
        
        // flag to deactivate "Has Player Shown?" checkbox in the InputForm
        this.playerHasShown = playerHasShown;
    }
}

export default Round;
