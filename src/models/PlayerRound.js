class PlayerRound {
    // Represents player's data in a particular round
    constructor(roundId, didShow, hasSeenMaal, pointsInThisRound){
        this.roundId = roundId;
        this.didShow = didShow;
        this.hasSeenMaal = hasSeenMaal;
        this.pointInThisRound = pointsInThisRound; // represents the total mal user had
        this.pointsInThisRoundAfterDeductions = 0; // represents the total point after all the mal calculation is done
        this.totalCummulativePointsAfterDeduction = 0; // represents the cummulative sum of total point a player has till now, from each round
    }
}

export default PlayerRound;
