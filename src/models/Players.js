class Player {
    constructor(id, name){
        // unique identifier for a Player
        this.id = id;
        // name of the Player
        this.name = name;
        // Rounds this player has played
        this.round = [];
    }
}

export default Player;
