import React from 'react';


class Header extends React.Component {
    constructor() {
        super();
        this.state = {color: "Red"};
    }

    render(){
        return <h2>This is a react component with {this.state.color} color</h2>
    }
}

export default Header;