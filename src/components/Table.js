import React from 'react';
import Table from 'react-bootstrap/Table';
//import Header from './Header'; 

class DataTable extends React.Component {
    getRows(){
        let rows = [];
        for(let i = 0; i < this.props.rows; i++){
            let columns = [];
            columns.push(<td>{i + 1}</td>)
            for(let j=0; j < this.props.columns; j++){
                columns.push(<td key={j}>-</td>)
            }
            rows.push(<tr key={i}>{columns}</tr>)
            
        }
        return rows
    }

    getHeader(){
        let header = [];
        header.push(<th>Round</th>)
        
        for(let i=1; i<= this.props.columns; i++){
            header.push(<th key={i}>Player {i}</th>)
        }

        return <tr>{header}</tr>
    }

    render(){
        let rows = this.getRows();
        let header = this.getHeader();
        return (
            <Table striped bordered hover>
                <thead>
                    {header}
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </Table>
        );
    }
}

export default DataTable;
