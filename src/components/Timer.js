import React from 'react';


class Timer extends React.Component {
    constructor() {
        super();
        this.state = {count: 0};
    }

    render(){
        return (
            <div>
                <h2>The time is {this.state.count}</h2>
                <button onClick={this.startCount}>Start Timer</button>
                <button onClick={this.stopCount}>Stop Timer</button>
                <button onClick={this.resetCount}>Reset Timer</button>
            </div>
        );
    }
    
    increment() {
      this.setState(state => ({
        count: this.state.count + 1
      }));
    }
  
    startCount = () => {
        this.counter = setInterval(() => this.increment(), 1000);
    }

    stopCount = () => {
        clearInterval(this.counter)
    }

    resetCount = () => {
        this.stopCount();
        this.setState(state => ({
            count: 0
          }));
    }
}

export default Timer;