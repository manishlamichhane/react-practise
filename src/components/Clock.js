import React from 'react';


class Clock extends React.Component {
    constructor() {
        super();
        this.state = {date: new Date().toLocaleTimeString()};
    }

    render(){
        return <h2>The time is {this.state.date}</h2>
    }
    
    tick() {
      this.setState(state => ({
        date: new Date().toLocaleTimeString()
      }));
    }
  
    componentDidMount() {
      setInterval(() => this.tick(), 1000);
    }
}

export default Clock;