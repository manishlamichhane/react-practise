import PointsModal from './PointsModal';


function PointsModalTableButton(props){

    return (
        <div>
            <PointsModal 
                onClose={props.onClose}
                show={props.showPointsModal} 
                currentRound={props.currentRound}
                currentPlayer={props.currentPlayer}
                submitPoints={props.submitPoints}
                aPlayerHasAlreadyShown={props.aPlayerHasAlreadyShown}
            />
            {props.marriageTable}
        </div>
    )
}

export default PointsModalTableButton;
