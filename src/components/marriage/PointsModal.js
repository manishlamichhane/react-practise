import React from 'react';
import Modal from 'react-bootstrap/Modal';

import InputForm from './PointsForm';


function PointsModal(props) {
  // hideModal calls hidePointsModal on AdvanceTable component
  // this way a central component maintains a source of truth  
  const hideModal = () => {
      props.onClose();
    }

    return (
      <>
        <Modal
          show={props.show}
          onHide={() => hideModal(false)}
          backdrop="static"
          keyboard={false}
          dialogClassName="modal-90w"
          aria-labelledby="example-custom-modal-styling-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-custom-modal-styling-title"> 
              Round: {props.currentRound} Player: {props.currentPlayer.name}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <InputForm 
              currentPlayer={props.currentPlayer} 
              currentRound={props.currentRound} 
              submitPoints={props.submitPoints} 
              hideModal={hideModal} 
              aPlayerHasAlreadyShown={props.aPlayerHasAlreadyShown} 
            />
          </Modal.Body>
        </Modal>
      </>
    );
  }

  export default PointsModal;
