import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


export default function InputForm(props) {
    const [playerDidShow, setPlayerDidShow] = React.useState(false);
    const [hasSeenMaal, setHasSeenMaal] = React.useState(false);
    const [points, setPoiints] = React.useState(false);
    const handleSubmit = (event) => {
        event.preventDefault(); // stop form submission
    
        let userInput = {
            pointsThisRound: points,
            hasSeenMaal: hasSeenMaal,
            playerDidShow: playerDidShow

        };

        //close the PointsModal by calling PointsModals hideModal method "leaked" via props
        props.hideModal();

        // return the points to AdvancedTable's submitPoints() which is "leaked" via PointsModals props
        // this was absolutely crazy idea for me!!!
        props.submitPoints(userInput, props.currentRound, props.currentPlayer);
    };

    const handleHasSeenMaalChange = (event) => {
        setHasSeenMaal(event.target.checked);
    };

    const handlePlayerDidShowChange = (event) => {
        setPlayerDidShow(event.target.checked);
    };

    const handlePointsChange = (event) => {
        setPoiints(event.target.value);
    };
  
    return (
        <Form noValidate validated={false}>
            <Form.Group as={Row} controlId="formHorizontalPassword">
                <Col sm={10}>
                    <Form.Check type="checkbox" id="hasSeenMaal" onChange={handleHasSeenMaalChange} label={`Did ${props.currentPlayer.name} see Maal?`} />
                </Col>
            </Form.Group>
            
            <Form.Group as={Row} controlId="formHorizontalPassword">
                <Col sm={10}>
                    <Form.Check 
                    type="checkbox" 
                    id="playerDidShow" 
                    onChange={handlePlayerDidShowChange} 
                    label={`Did ${props.currentPlayer.name} do Show?`} 
                    disabled={props.aPlayerHasAlreadyShown}
                    />
                </Col>
            </Form.Group>
            
            <Form.Group as={Row} controlId="formHorizontalPassword">
                <Col sm={10}>
                    <Form.Label>How many points did {props.currentPlayer.name} get?</Form.Label>
                </Col>
                <Col sm={10}>
                    <Form.Control 
                    type="input" 
                    id="points" 
                    onChange={handlePointsChange} 
                />
                </Col>
            </Form.Group>
            
        
            <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                    <Button type="submit" onClick={handleSubmit}>Submit</Button>
                </Col>
            </Form.Group>
      </Form>
    );
  }
  