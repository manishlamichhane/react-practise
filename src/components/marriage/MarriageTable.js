import React from 'react';
import Table from 'react-bootstrap/Table';


function MarriageTable(props){
    return (
        <Table striped bordered hover size="sm">
        <thead>
            {props.header}
        </thead>
        <tbody>
            {props.rows}
        </tbody>
        </Table>
    )
}

export default MarriageTable;
