import React from 'react';

import PlayerController from '../../controllers/PlayerControllers';
import RoundController from '../../controllers/RoundController';
import Round from '../../models/Round';
import TableController from '../../controllers/TableController';
import PointsModalTableButton from './PointsModalTableButton';
import MarriageTableWithButtons from './MarriageTableWithButtons';


class AdvanceTable extends React.Component {
    constructor(props) {
        super(props);
        this.pĺayerController = new PlayerController(props.columns); 
        this.roundController = new RoundController();

        this.state = {
            // represents the current players
            players: this.pĺayerController.initialize(),
            
            // array of rounds. rounds have their id and totalPoints, which is a sum of points from all the players of that round
            // this can be thought as a histroy of all the rounds played so far
            rounds: [
                new Round(props.rows, 0, false)
            ],

            // pointsModal is to be shown only when a column in a round of a particular player is clicked.
            // when the click event happens, this showPointsModal value is set to true
            showPointsModal: false,

            // determines what round is currently clicked. Its used to identify a round to assign user input from PointsModal
            currentClickedRound: null,
            // determines what player is currently clicked. Its used to identify a player to assign user input from PointsModal
            currentClickedPlayer: null
        };
    }

    updatePlayerName = (player) => {
        let updatedPlayer = this.pĺayerController.updatePlayerName(player);
        
        // adhering to immutability
        // even though the name of a player is changed, we replace the entire players state
        let players = [...this.state.players];

        // IMPORTANT NOTE: Player's id is players key in the array + 1. More info in: initializerPlayers()
        let playerKeyInArray = player.id - 1;
        players[playerKeyInArray] = updatedPlayer;
        
        this.setState({players: players});

    }

    // defining it as an arrow function, since it will be called from TableController
    // "this" in the function below would refer to TableController if it's not defined
    // as arrow function
    showPointsModal = (roundNumber, playerId) => {
        let showPointsModal = this.state.showPointsModal;
        
        let currentPlayerIndex = playerId - 1
        let currentClickedPlayer = this.state.players[currentPlayerIndex];
        
        this.setState({
            showPointsModal: !showPointsModal,
            currentClickedRound: roundNumber,
            currentClickedPlayer: currentClickedPlayer
        });

    }

    hidePointsModal = () => {
        this.setState({
            showPointsModal: false,
            currentClickedRound: null,
            currentClickedPlayer: null,
        });
    }

    createNewRoundRow = () => {
        // React says use immutability :-]
        let rounds = [...this.state.rounds];
        const lastestRound = rounds[this.state.rounds.length - 1];
        //Round is a DataClass that has a history of rounds and total points
        // maybe rename to something else
        const newRound = new Round(lastestRound.id + 1, 0);
        rounds.push(newRound)
        // this causes re-rendering of the component
        // which adds a new row to the table
        this.setState({rounds: rounds});
    }

    submitPoints = (pointsDetail, roundNumber, player) => {
        let newPlayerRound = this.roundController.createNewRound(pointsDetail, roundNumber);

        // replace the round instead of appending. This enables editing user points.
        player.round[roundNumber-1] = newPlayerRound;

        let players = [...this.state.players];
        players[player.id - 1] = player
        
        // whenever a player submits a point, a cummulative sum is calculated and saved in this.state.rounds object
        // this will come on handy later while doing final calculation.
        // I wont need to loop through the each players round info to get the final sum
        let rounds = [...this.state.rounds];
        let roundToBeReplaced = rounds[roundNumber - 1];
        
        // sets the flag that tells if a player has already shown in this round.
        // this flag is used to disable / enable "Player did show?" checkbox in InputForm
        let newRoundAttributes = [roundToBeReplaced.id, roundToBeReplaced.totalPoints + newPlayerRound.pointInThisRound];
        if(roundToBeReplaced.playerHasShown !==true && pointsDetail.playerDidShow === true) {
            // set the playerHasShown flag to true if the flag was false and the current point submission has playerDidShow as True. 
            newRoundAttributes.push(pointsDetail.playerDidShow);
        }else{
            // set the playerHasShown flag to what it was before this submission if player submitting point did not show.
            newRoundAttributes.push(roundToBeReplaced.playerHasShown);
        }
        
        // replacing the round object with new round
        let newRound = new Round(...newRoundAttributes);
        rounds[roundToBeReplaced.id - 1] = newRound;

        this.setState(
            {
                players: players,
                rounds: rounds
            }
        );
    }

    calculatePoint = () => {
        // calculates the sum of player points in a round
        // eases the calculation of points later
        try {
            let [rounds, players] = this.roundController.calculatePlayerPointsPerRound(this.state.rounds, this.state.players);
            this.setState(
                {
                    rounds: rounds, // there is no particular reason to update round here.
                    players: players
                }
            )
        }catch(exception) {
            alert(exception);
        }
        
    }

    render(){
        let marriageTable = new TableController(
            this.state.rounds.length, 
            this.state.players, 
            this.showPointsModal,
            this.updatePlayerName,
        ).createTable();

        console.log(this.state);
        
        // conditional rendering to render the Modal that is used to Collect points from user.
        // this is governed by the a state variable, which is set / unset by the PointsModal itself
        // using the concept of "lifting the state". 
        // The onClose props in the PointsModal "leaks" the control of the showPointsModal state variable
        // from this component (AdvancedTable) to PointsModal

        if (this.state.showPointsModal){
            let rounds = this.state.rounds;
            let aPlayerHasAlreadyShown = rounds[rounds.length - 1]?.playerHasShown || false;
            const pointsModalProps = {
                onClose: this.hidePointsModal,
                submitPoints: this.submitPoints,
                showPointsModal: this.state.showPointsModal,
                currentRound: this.state.currentClickedRound,
                currentPlayer: this.state.currentClickedPlayer,
                aPlayerHasAlreadyShown: aPlayerHasAlreadyShown,
            }
            return <PointsModalTableButton 
                     table={marriageTable} 
                     {...pointsModalProps} 
                    />
        }
        
        return <MarriageTableWithButtons 
            table={marriageTable}
            createNewRoundRow={this.createNewRoundRow}
            calculatePoint={this.calculatePoint}
        />
    }
}

export default AdvanceTable;
