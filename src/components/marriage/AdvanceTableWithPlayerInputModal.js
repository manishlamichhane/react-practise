import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import AdvanceTable from './AdvanceTable';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';



function PlayerInputForm(props) {
  let options = [2, 3, 4, 5];
  let [players, setPlayers] = useState(2);
  
  const handleSelection = (event) => {
    let playerInputInNumber = +event.target.value; // not sure if this is cool or funny! 
    setPlayers(playerInputInNumber);
  }

  const handleSubmit = (event) => {
    event.preventDefault(); // stop form submission
    // passes the current state to parent by using the parent binding provided in props
    props.setNumberOfPlayers(players);
    props.showModal(false);
  }

  return (
    <Form>
      <Form.Group controlId="exampleForm.ControlSelect1">
        <Form.Label>Select the number of players: </Form.Label>
        <Form.Control as="select" onChange={handleSelection}>
          {options.map((value, index) => {
            return <option key={index} value={value}>{value}</option>
          })}
        </Form.Control>
      </Form.Group>
      <Form.Group as={Row}>
          <Col sm={{ span: 10, offset: 2 }}>
              <Button type="submit" onClick={handleSubmit}>Submit</Button>
          </Col>
      </Form.Group>
  </Form>
  )
}

function PlayerInputModal(props) {
  const [show, setShow] = useState(true);

  const handleClose = () => setShow(false);

    return (
      <>
        <Modal
          show={show}
          onHide={handleClose}
          dialogClassName="modal-90w"
          backdrop="static"
          keyboard={false}
          aria-labelledby="example-custom-modal-styling-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-custom-modal-styling-title"> 
              Welcome to Marriage point calculator you Juwades!!!
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <PlayerInputForm setNumberOfPlayers={props.setNumberOfPlayers} showModal={setShow}/>
          </Modal.Body>
        </Modal>
      </>
    );
  }


function MarriageGame(props) {
  const [numberOfPlayers, setNumberOfPlayers] = useState(0);
  const DEFAULT_NUMBER_OF_ROWS = 1;

    return (
      <div>
        <PlayerInputModal setNumberOfPlayers={setNumberOfPlayers} />
        {numberOfPlayers > 0 && <AdvanceTable rows={DEFAULT_NUMBER_OF_ROWS} columns={numberOfPlayers} />}
      </div>
    )
}

export default MarriageGame;
