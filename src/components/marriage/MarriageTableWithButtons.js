import { Button } from 'react-bootstrap';


function MarriageTableWithButtons(props){
    return (
        <div>
            {props.table}
            <Button variant="danger" onClick={props.createNewRoundRow}>Create New Round</Button>{' '}
            <Button variant="danger" onClick={props.calculatePoint}>Calculate Points</Button>{' '}
        </div>
    )
}

export default MarriageTableWithButtons;
